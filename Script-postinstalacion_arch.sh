#!/usr/bin/env sh
sudo pacman --noconfirm -Syu
sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
sudo pacman-key --lsign-key FBA220DFC880C036
sudo pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
sudo sed -i '$ a \\n[chaotic-aur]\nInclude = /etc/pacman.d/chaotic-mirrorlist' /etc/pacman.conf
sudo pacman -S fish emacs-wayland fd ripgrep
chsh -s /usr/bin/fish $USER
sudo pacman -S --needed --noconfirm git base-devel && git clone https://aur.archlinux.org/paru.git && cd yay && makepkg -si
cd
sudo pacman -S hunspell hunspell-es --noconfirm --needed
paru -S hunspell-eo
sudo pacman -S papirus-icon-theme materia-kde materia-gtk-theme syncthing syncthingtray-qt6 ttf-jetbrains-mono-nerd --noconfrim --needed
systemctl enable --now syncthing@$USER.service
sudo pacman -S zip p7zip --needed --noconfirm
sudo pacman -S libreoffice-fresh libreoffice-fresh-es --needed --noconfirm
sudo pacman -S kleopatra firefox firefox-i18n-es-es firefox-i18n-eo --needed --noconfirm
git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.config/emacs
~/.config/emacs/bin/doom install
